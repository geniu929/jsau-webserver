const express = require('express')
const nunjucks = require('nunjucks')
const path = require('path')
const app = express()
app.use(express.static(path.resolve(__dirname,'../jsau-webapp')))

nunjucks.configure(path.resolve(__dirname,'../jsau-webapp'), {
    autoescape: true,
    express: app
})

app.get('*', function(req, res) {
    res.render("index.html")
})

const port = process.env.PORT || 3001
app.listen(port, () => {
    console.log(`Le serveur est lancé et vous écoute sur le port :${port}`)
})
